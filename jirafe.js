(function() {
  var d=document,g=d.createElement('script'),s=d.getElementsByTagName('script')[0];
  g.type='text/javascript';g.defer=g.async=true;g.src=d.location.protocol+'//beacon.jirafe.com/jirafe_api.js';
  s.parentNode.insertBefore(g,s);
})();

function jirafe_deferred(jirafe_api) {
  var settings = Drupal.settings.jirafe;
  var data = {
    customer: {
      id: settings.uid
    }
  };
  data[settings.type] = settings;
  jirafe_api.pageview('', settings.site_id, settings.type, data);
}
