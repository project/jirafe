INTRODUCTION
------------
The Jirafe module provides integration with the 
jirafe.com services. 

REQUIREMENTS
------------
The module require commerce_order module to be enabled. 
Commerce order is a sub-module of the commerce module.:
  * Commerce Order (https://www.drupal.org/project/commerce)
  
  
INSTALLATION
------------
1.  First you need to download and enable the module. 
    You can do that by going to https://www.drupal.org/project/jirafe
    And downloading the module from there. Then you should place the un-archived 
    package in your_site/sites/all/modules/
2.  Enable the module from your_site/admin/modules
3.  Enabling the module will create menu item for the module configuration settings 
    under Store settings->Advanced store settings (your_site/admin/commerce/config/jirafe)
4   If you go to jirafe settings page you will be required to enter your credentials: 
    Site id and Access token, if you do not have login credentials, 
    you can always go to https://jirafe.com/ and sign up for.

CONFIGURATION
-------------
When the module is enabled and your credentials are in place, 
you will be able to configure the jirafe module to send data to jirafe.com.
1.  To do that first you need to map some fields to help jirafe to 
    recognize the metadata of your products. Under categories are presented all the 
    commerce fields in drupal database from the list you can select any field you want to be mapped, 
    for example if you select field_bag_size, the module will look for 
    products in the orders that have that field.
2.  From the select box for catalog and brand you can select fields related to 
    particular product catalog and brand.
3.  The last field to be mapped is product images field. Currently the module is design to map to 
    field_images and field_image, but commerce_product module uses by default field_images, 
    so you need to select this one, although if you’ve done some modifications and 
    your products have field_image you should switch to that field. 
4.  The last thing to do is to select what kind of historic information to send to jirafe. 
    For that go to ‘Historic data’ tab, from here you can select the three different types of data
    to be send over to jirafe Orders, Products or/and Customers data. 
    If you press the Send data button this will trigger a batch process that will send 
    all the data that has been queued for those entities. 
    This data will be send also on cron run.
5.  It is recomended the users of this module set the cron to run every 5 minutes 
    you can achieve that by installing the ultimate_cron module and after that go 
    to your_site/admin/config/system/cron/scheduler/crontab 
    there in the rules field you should enter */5 * * * * this will set the cron jobs 
    to run every 5 minutes.