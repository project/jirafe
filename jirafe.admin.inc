<?php

/**
 * @file
 * Configuration for Jirafe module.
 */

/**
 * Displays Jirafe configuration.
 */
function jirafe_admin_settings() {
  $options = array(
    'references' => array(t('none')),
    'images' => array(t('none')),
  );

  foreach (field_info_fields() as $field_name => $field) {
    if ($field['type'] == 'taxonomy_term_reference' || $field['type'] == 'entityreference') {
      $options['references'][$field_name] = $field_name;
    }
    elseif ($field['type'] == 'image') {
      $options['images'][$field_name] = $field_name;
    }
  }

  $form['jirafe_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#default_value' => variable_get('jirafe_site_id', ''),
    '#required' => TRUE,
  );
  /*
  $form['jirafe_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#default_value' => variable_get('jirafe_client_id', ''),
    '#required' => TRUE,
  );
  $form['jirafe_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client secret'),
    '#default_value' => variable_get('jirafe_client_secret', ''),
    '#required' => TRUE,
  );
  */
  $form['jirafe_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access token'),
    '#default_value' => variable_get('jirafe_access_token', ''),
    '#required' => TRUE,
  );
  $form['mapping'] = array(
  	'#type' => 'fieldset',
    '#title' => t('Field mapping'),
    '#description' => t('These fields are helping Jirafe to recognise the metadata of your products and categories better.'),
  );
  $form['mapping']['jirafe_mapping_categories'] = array(
    '#type' => 'select',
    '#title' => t('Categories'),
    '#options' => $options['references'],
    '#multiple' => TRUE,
    '#default_value' => variable_get('jirafe_mapping_categories', ''),
  );
  $form['mapping']['jirafe_mapping_catalog'] = array(
    '#type' => 'select',
    '#title' => t('Catalog'),
    '#options' => $options['references'],
    '#default_value' => variable_get('jirafe_mapping_catalog', ''),
  );
  $form['mapping']['jirafe_mapping_brand'] = array(
    '#type' => 'select',
    '#title' => t('Brand'),
    '#options' => $options['references'],
    '#default_value' => variable_get('jirafe_mapping_brand', ''),
  );
  $form['mapping']['jirafe_mapping_images'] = array(
    '#type' => 'select',
    '#title' => t('Product images'),
    '#options' => $options['images'],
    '#default_value' => variable_get('jirafe_mapping_images', ''),
  );

  return system_settings_form($form);
}

function jirafe_admin_settings_validate($form, $form_state) {
  $user = $GLOBALS['user'];

  if (!empty($form_state['values']['jirafe_site_id']) && !empty($form_state['values']['jirafe_access_token'])) {
    $data = array(
      'id' => $user->uid,
      'active_flag' => TRUE,
      'create_date' => date('c', $user->created),
      'change_date' => date('c', $user->login),
      'first_name' => $user->name,
      'email' => $user->mail,
    );

    $options = array(
      'method' => 'POST',
      'data' => json_encode($data),
      'headers' => array(
        'Authorization' => 'Bearer ' . $form_state['values']['jirafe_access_token'],
        'Content-type' => 'application/json',
      ),
    );

    $request = drupal_http_request('https://event.jirafe.com/v2/' . $form_state['values']['jirafe_site_id'] . '/employee', $options);
    $response = json_decode($request->data);

    if (!$response->success) {
      form_set_error('', check_plain($response->message));
    }
  }
}

/**
 * Form to send historic data to batch endpoint.
 */
function jirafe_admin_historic() {

  $form['help'] = array(
  	'#markup' => '<p>' . t('You can send your historic order data to Jirafe by clicking the button below. You might use it just once, when starting using Jirafe.') . '</p>'
  );
  $form['types'] = array(
    '#type' => 'checkboxes',
    '#default_value' => array('order', 'product', 'customer'),
    '#options' => array(
      'order' => t('Orders'),
      'product' => t('Products'),
      'customer' => t('Customers'),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send data'),
  );
  if ($last = variable_get('jirafe_last_historic')) {
    $form['last'] = array(
      '#markup' => '<p>' . t('Last used @t ago', array('@t' => format_interval(REQUEST_TIME - $last))) . '</p>',
    );
  }

  return $form;
}

/**
 * Submit callback for historic data form.
 */
function jirafe_admin_historic_submit($form, $form_state) {
  foreach ($form_state['values']['types'] as $data_type) {
    $batch = array(
      'title' => t('Sending data to Jirafe.'),
      'operations' => array(),
      'finished' => 'jirafe_batch_finished_' . $data_type,
      'file' => drupal_get_path('module', 'jirafe') . '/jirafe.admin.inc',
    );

    if ($data_type) {
      switch ($data_type) {
        case 'order':
          foreach (commerce_order_load_multiple('', array('status' => 'completed')) as $order) {
             $batch['operations'][] = array('jirafe_batch_operation', array($order, $data_type));
	        }
          break;

        case 'product':
	        // Look up any node for a product reference to avoid errors.
          $product_ids = jirafe_commerce_products_with_display();
          foreach (commerce_product_load_multiple($product_ids, array('status' => 1)) as $product) {
            $batch['operations'][] = array('jirafe_batch_operation', array($product, $data_type));
		      }
          break;

        case 'customer':
          $query = db_select('commerce_order', 'c');
          $query->distinct();
          $query->addField('c', 'uid');
          $query->condition('status', 'completed');
          $query->condition('c.uid', 0, '!=');
          $users = $query->execute()->fetchCol();

          foreach ($users as $uid) {
            $batch['operations'][] = array('jirafe_batch_operation', array($uid, $data_type));
          }
          break;
      }

      $_SESSION['jirafe_batch'] = array('type' => 'batch', 'failed' => array());
      batch_set($batch);
    }
  }
}

/**
 * Batch operation: collecting order data and sending chunks of it to Jirafe.
 */
function jirafe_batch_operation($entity, $data_type, &$context) {
  $type_ids = array(
    'order' => 'order_id',
    'product' => 'product_id',
    'customer' => 'uid',
  );

  try {

    $callback = 'jirafe_' . $data_type . '_data';
    $_SESSION['jirafe_batch'][$data_type][] = $callback($entity);

    $json = json_encode($_SESSION['jirafe_batch']);
    $json_size = function_exists('mb_strlen') ? mb_strlen($json, '8bit') : strlen($json);

    if ($json_size > JIRAFE_ALLOWED_DATA_SIZE) {
      jirafe_send_queued_data($_SESSION['jirafe_batch']);
      jirafe_reset_session_data();
    }

    $context['results'][] = $entity->{$type_ids[$data_type]};
    $context['message'] = t('Processing @ds', array('@d' => $data_type));
  }
  catch (Exception $e) {
    watchdog('jirafe', t('Cannot send @d @i to Jirafe. A following error occured: @e', array(
      '@i' => $entity->{$type_ids[$data_type]},
      '@e' => $e,
    )));
    $_SESSION['jirafe_batch']['failed'][] = $entity->{$type_ids[$data_type]};
  }
}

/**
 * No way to pass arguments to the finish callback, so have to use 3 different functions.
 */
function jirafe_batch_finished_order($success, $results, $operations) {
  jirafe_batch_finished($success, $results, 'order');
}

function jirafe_batch_finished_product($success, $results, $operations) {
  jirafe_batch_finished($success, $results, 'product');
}

function jirafe_batch_finished_customer($success, $results, $operations) {
  jirafe_batch_finished($success, $results, 'customer');
}

/**
 * Finish callback for batch process.
 */
function jirafe_batch_finished($success, $results, $data_type) {
  if ($success) {
    jirafe_send_queued_data($_SESSION['jirafe_batch']);
    jirafe_reset_session_data();
    variable_set('jirafe_last_historic', REQUEST_TIME);
    drupal_set_message(t('Historic data of @s @ts successfuly sent to Jirafe. Failed: @f.', array(
      '@t' => $data_type,
      '@s' => count($results),
      '@f' => count($_SESSION['jirafe_batch']['failed']),
    )));
    $_SESSION['jirafe_batch']['failed'] = array();
  }
  else {
    drupal_set_message(t('An error occured while sending data to Jirafe'), 'error');
  }
}

/**
 * Reset the session data.
 */
function jirafe_reset_session_data() {
  $_SESSION['jirafe_batch']['order'] = array();
  $_SESSION['jirafe_batch']['product'] = array();
  $_SESSION['jirafe_batch']['customer'] = array();
}
